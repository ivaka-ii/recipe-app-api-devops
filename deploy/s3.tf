resource "aws_s3_bucket" "app_public_files_ivaka" {
  bucket        = "${local.prefix}-files-ivaka"
  acl           = "public-read"
  force_destroy = true
}